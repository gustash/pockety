package com.gustash.pockety;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;

public class LoadingDialog {

    private ProgressDialog dialog;

    public LoadingDialog(Context context) {
        dialog = new ProgressDialog(context);
        dialog.setTitle("Loading");
        dialog.setCancelable(false);
    }

    public void startLoading() {
        dialog.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if (isLoading()) {
                    dialog.dismiss();
                }
            }
        }, 5000);  // dismiss dialog after 5000 milliseconds
    }

    public void stopLoading() {
        if (isLoading()) {
            dialog.dismiss();
        }
    }

    private boolean isLoading() {
        return dialog.isShowing();
    }
}
