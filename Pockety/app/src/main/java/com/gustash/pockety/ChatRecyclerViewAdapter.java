package com.gustash.pockety;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<ChatRecyclerViewAdapter.JobViewHolder> {

    public static class JobViewHolder extends RecyclerView.ViewHolder {
        TextView message;
        ImageView sender;
        Button loadMoreButton;

        JobViewHolder(final View itemView) {
            super(itemView);
            sender = (ImageView)itemView.findViewById(R.id.inChatAvatar);
            message = (TextView)itemView.findViewById(R.id.inChatMessage);
            loadMoreButton = (Button)itemView.findViewById(R.id.load_more);
        }
    }

    final int CURRENT_USER = 0;
    final int OTHER_USER = 1;
    final int LOAD_MORE = 2;

    List<ParseObject> messages;
    ParseUser sender;
    String currentUserID = ParseUser.getCurrentUser().getObjectId();
    ParseFile senderImage;
    View v;

    ChatRecyclerViewAdapter(List<ParseObject> messages){
        this.messages = messages;
    }

    @Override
    public ChatRecyclerViewAdapter.JobViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case CURRENT_USER: {
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item_current_user, viewGroup, false);
                return new JobViewHolder(v);
            }
            case OTHER_USER: {
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_item_other_user, viewGroup, false);
                return new JobViewHolder(v);
            }
            case LOAD_MORE: {
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.load_more_row, viewGroup, false);
                return new JobViewHolder(v);
            }
        }

        return null;
    }

    @Override
    public int getItemViewType(int position) {
        boolean isCurrentUser;
        try {
            isCurrentUser= messages.get(position).getParseUser("sender")
                    .getObjectId().equals(currentUserID);
        } catch (Exception e) {
            isCurrentUser = false;
        }

        int i;
        if (isHasLoadButton()) { i = 1; } else { i = 0; }

        if (isCurrentUser && position < getItemCount() - i) {
            return CURRENT_USER;
        } else if (!isCurrentUser && position < getItemCount() - i) {
            return OTHER_USER;
        } else {
            return LOAD_MORE;
        }
    }

    @Override
    public void onBindViewHolder(final ChatRecyclerViewAdapter.JobViewHolder holder, int position) {
        int i;
        if (isHasLoadButton()) { i = 1; } else { i = 0; }

        if (position >= getItemCount() - i) {
            final int lastItemPostition = position - 1;
            holder.loadMoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("LOAD", "Load more");
                    ChatActivity.loadMoreMessages(messages.get(lastItemPostition).getCreatedAt());
                }
            });
        } else {
            sender = messages.get(position).getParseUser("sender");

            String message = messages.get(position).getString("message");
            holder.message.setText(message);

            senderImage = sender.getParseFile("avatar");
            Picasso.with(v.getContext()).load(senderImage.getUrl()).into(holder.sender);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }

    public void addMessage(ParseObject message) {
        messages.add(0, message);
        notifyDataSetChanged();
    }

    public void addMessages(List<ParseObject> _messages) {
        messages.addAll(_messages);
        notifyDataSetChanged();
    }

    private boolean hasLoadButton = true;

    public boolean isHasLoadButton() {
        return hasLoadButton;
    }

    public void setHasLoadButton(boolean hasLoadButton) {
        this.hasLoadButton = hasLoadButton;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (hasLoadButton && messages != null) {
            return messages.size() + 1;
        } else if (!hasLoadButton && messages != null) {
            return messages.size();
        } else {
            return 0;
        }
    }
}
