package com.gustash.pockety;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

class MyPagerAdapter extends FragmentStatePagerAdapter {

    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (getPageTitle(position) == "Demand") {
            return MyDemandFragment.newInstance(position);
        } else {
            return MyOfferFragment.newInstance(position);
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) { return "Demand"; }
        else { return "Offer"; }
    }
}
