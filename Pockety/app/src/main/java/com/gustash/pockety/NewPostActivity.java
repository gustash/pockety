package com.gustash.pockety;

import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.model.LatLng;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class NewPostActivity extends AppCompatActivity {

    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;

    EditText etTitle, etDescription, etLocation, etPrice;
    SwitchCompat scPhone, scEmail/*, scTrade*/;
    RadioGroup radioGroup;
    LoadingDialog dialog;
    LatLng postLocation;
    String current = "";
    boolean featurePost = false;
    LinearLayout featurePostLayout;
    ImageView featurePostStar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);

        dialog = new LoadingDialog(this);

        etTitle = (EditText) findViewById(R.id.titleEditText);
        etPrice = (EditText) findViewById(R.id.priceEditText);
        etDescription = (EditText) findViewById(R.id.descriptionEditText);
        etLocation = (EditText) findViewById(R.id.locationEditText);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        scPhone = (SwitchCompat) findViewById(R.id.phoneSwitchCompat);
        scEmail = (SwitchCompat) findViewById(R.id.emailSwitchCompat);
        featurePostLayout = (LinearLayout) findViewById(R.id.feature_post_linear_layout);
        featurePostStar = (ImageView) findViewById(R.id.feature_post_star);
        //scTrade = (SwitchCompat) findViewById(R.id.tradeSwitchCompat);

        etPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals(current)){
                    etPrice.removeTextChangedListener(this);

                    DecimalFormat precision = new DecimalFormat("0.00");
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setDecimalSeparator(',');
                    symbols.setGroupingSeparator('.');
                    precision.setDecimalFormatSymbols(symbols);

                    String cleanString = s.toString().replaceAll("[,.€\\s+]", "");

                    double parsed = Double.parseDouble(cleanString);
                    //String formatted = NumberFormat.getCurrencyInstance().format((parsed / 100));
                    String formatted = NumberFormat.getCurrencyInstance(Locale.ITALY)
                            .format(parsed / 100);

                    current = formatted;
                    etPrice.setText(formatted);
                    etPrice.setSelection(formatted.length());

                    etPrice.addTextChangedListener(this);
                }
            }
        });

        etLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                            .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                            .build();


                    Intent intent =
                            new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                                    .setFilter(typeFilter)
                                    .build(NewPostActivity.this);
                    startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);
                } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
                    // TODO: Handle the error.
                    e.printStackTrace();
                }

            }
        });

        featurePostLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (featurePost) {
                    featurePostStar.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.ic_star_border_black_36dp, null));
                    featurePost = false;
                } else {
                    featurePostStar.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.ic_star_black_36dp, null));
                    featurePost = true;
                }
            }
        });
    }

    private boolean editTextIsEmpty(EditText editText) {
        return editText.getText().toString().equals("");
    }

    private boolean textIsNum(String text) {
        try {
            text = text.replaceAll("[.€\\s+]", "");
            text = text.replace(",", ".");
            Double.parseDouble(text);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean radioButtonIsChecked(RadioGroup _radioGroup) {
        return _radioGroup.getCheckedRadioButtonId() > -1;
    }

    private double convertToDouble(String text) {
        text = text.replaceAll("[.€\\s+]", "");
        text = text.replace(",", ".");
        return Double.parseDouble(text);
    }

    public void post () {
        //TODO: Post location

        Log.d("CHECKING", "Validating fields");

        if (!radioButtonIsChecked(radioGroup) ||
                editTextIsEmpty(etTitle) ||
                editTextIsEmpty(etPrice) ||
                !textIsNum(etPrice.getText().toString()) ||
                editTextIsEmpty(etDescription) ||
                editTextIsEmpty(etLocation)) {
            return;
        }

        Log.d("SUCCESS", "Successfully validated fields");

        String title = etTitle.getText().toString(),
                description = etDescription.getText().toString(),
                type;
        double price = convertToDouble(etPrice.getText().toString());
        boolean showPhone = scPhone.isChecked(), showEmail = scEmail.isChecked()/*,
                isTrade = scTrade.isChecked()*/;

        int selectedID = radioGroup.getCheckedRadioButtonId();
        RadioButton chosenOption = (RadioButton) findViewById(selectedID);
        int index = radioGroup.indexOfChild(chosenOption); //index is 0 for demand and 1 for offer

        String locality = etLocation.getText().toString();
        double lat = postLocation.latitude;
        double lon = postLocation.longitude;

        if (index == 0) { type = "d"; } else { type = "o"; }

        ParseObject post = new ParseObject("Post");
        post.put("title", title);
        post.put("title_lowercase", title.toLowerCase());
        post.put("description", description);
        post.put("description_lowercase", description.toLowerCase());
        post.put("price", price);
        post.put("location", new ParseGeoPoint(lat, lon));
        post.put("locality", locality);
        post.put("showPhone", showPhone);
        post.put("showEmail", showEmail);
        //post.put("isTrade", isTrade);
        post.put("isFinished", false);
        post.put("isFeatured", featurePost);
        post.put("type", type);
        post.put("parent", ParseUser.getCurrentUser());
        dialog.startLoading();
        post.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                dialog.stopLoading();
                if (e == null) {
                    Intent intent = new Intent();
                    setResult(RESULT_CANCELED, intent);
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "There was a problem posting, please try again", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_new_post, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post) {
            post();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(this, data);
                Log.i("PLACE", place.getName().toString());

                etLocation.setText(place.getName());
                postLocation = place.getLatLng();
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                // TODO: Handle the error.
                Log.i("STATUS", status.getStatusMessage());

            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }
}
