package com.gustash.pockety;

import android.app.SearchManager;
import android.content.Intent;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import ibt.ortc.api.Ortc;


public class MainActivity extends AppCompatActivity {

    private String userID;

    public static double latitude, longitude;
    boolean isAdapterSet = false;
    //static GoogleApiClient mGoogleApiClient;
    View v;

    static FloatingActionButton fab;
    AdView mAdView;

    private Toolbar toolbar;
    private static TabLayout mTabLayout;
    private static ViewPager mPager;
    private static MyPagerAdapter mPagerAdapter;
    String TITLES[] = {"Chats", "Settings", "Logout"};
    int ICONS[] = {R.drawable.ic_chat_black_24dp, R.drawable.ic_settings_black_24dp, R.drawable.ic_power_settings_new_black_24dp};

    String NAME;
    String EMAIL;
    String PROFILE;

    RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;
    RecyclerView.LayoutManager mLayoutManager;
    DrawerLayout Drawer;
    boolean isDrawerOpened;
    MySharedPreferences mySharedPreferences;

    ActionBarDrawerToggle mDrawerToggle;

    static boolean isSearching = false;
    static String searchString;

    LoadingDialog loadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);

        loadingDialog = new LoadingDialog(MainActivity.this);

        mySharedPreferences = new MySharedPreferences(this);
        checkIfFirstRun();

        if (authenticate()) {
            ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject parseObject, ParseException e) {
                    if (e == null) {
                        if (parseObject.getParseGeoPoint("location") == null) {

                            //If current saved location is 0.0, 0.0 then user hasn't set a location
                            startActivity(new Intent(MainActivity.this, MapsActivity.class));
                            finish();
                        } else {
                            proceedLoading();
                        }
                    } else {
                        e.printStackTrace();
                        finish();
                    }
                }
            });
        } else {
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }
    }

    private void checkIfFirstRun() {
        if (mySharedPreferences.isFirstTimeRunning()) {
            PreferenceManager.setDefaultValues(this, R.xml.settings, false);
            mySharedPreferences.setRanForFirstTime();
        }
    }

    private void proceedLoading() {
        mPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        LayoutInflater inflater = LayoutInflater.from(this);
        v = inflater.inflate(R.layout.main_fragment, null, false);

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mPager = (ViewPager) findViewById(R.id.pager);
        mTabLayout.setTabsFromPagerAdapter(mPagerAdapter);
        mAdView = (AdView) findViewById(R.id.mainActivityAdView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.simple_grow);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.startAnimation(animation);

        mPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mPager);
        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    public static void refresh(final int tab) {
        ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    if (MyOfferFragment.noneNearText.getVisibility() == View.VISIBLE) {
                        MyOfferFragment.noneNearText.setVisibility(View.GONE);
                        MyOfferFragment.swipeRefreshLayout.setVisibility(View.VISIBLE);
                    }
                    mPager.setAdapter(mPagerAdapter);
                    mPager.setCurrentItem(tab);
                } else {
                    e.printStackTrace();
                }
            }
        });
    }

    public static FloatingActionButton getFAB() {
        return fab;
    }

    private boolean authenticate() {
        return (ParseUser.getCurrentUser() != null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (authenticate()) {
            loadUserData();
            OrtcMethods.initializeSubscriptions(this, userID);
        }
    }

    private void loadUserData() {
        userID = ParseUser.getCurrentUser().getObjectId();
        NAME = ParseUser.getCurrentUser().getString("name");
        EMAIL = ParseUser.getCurrentUser().getEmail();
        PROFILE = ParseUser.getCurrentUser().getParseFile("avatar").getUrl();
        initializeDrawer();
    }

    private void initializeDrawer() {
        mRecyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mAdapter = new DrawerAdapter(TITLES,ICONS,NAME,EMAIL,PROFILE);
        mRecyclerView.setAdapter(mAdapter);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final GestureDetector mGestureDetector = new GestureDetector(MainActivity.this, new GestureDetector.SimpleOnGestureListener() {

            @Override public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

        });

        Drawer = (DrawerLayout) findViewById(R.id.DrawerLayout);
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.openDrawer,R.string.closeDrawer){
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                isDrawerOpened = true;
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                isDrawerOpened = false;
            }
        };

        Drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        mRecyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {
                View child = recyclerView.findChildViewUnder(motionEvent.getX(), motionEvent.getY());


                if (child != null && mGestureDetector.onTouchEvent(motionEvent)) {
                    Drawer.closeDrawers();
                    switch (recyclerView.getChildAdapterPosition(child)) {
                        case 1: {
                            startActivity(new Intent(getApplicationContext(), ChatRoomsActivity.class));
                            break;
                        }
                        case 2: {
                            startActivity(new Intent(getApplicationContext(), PreferencesActivity.class));
                            break;
                        }
                        case 3: {
                            loadingDialog.startLoading();
                            Log.d("IS SUBBED", String.valueOf(OrtcMethods.checkIfSubscribed(userID)));
                            OrtcMethods.unsubscribeAll(ParseUser.getCurrentUser().getObjectId());
                            ParseUser.logOutInBackground(new LogOutCallback() {
                                @Override
                                public void done(ParseException e) {
                                    loadingDialog.stopLoading();

                                    if (e == null) {
                                        startActivity(new Intent(MainActivity.this, LoginActivity.class));
                                        finish();
                                    } else {
                                        Toast.makeText(MainActivity.this,
                                                "Could not logout :/",
                                                Toast.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    }
                                }
                            });
                            break;
                        }
                    }
                    return true;

                }


                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView recyclerView, MotionEvent motionEvent) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });
    }

    private List<String> getSubscribedChannels() {

        final List<ParseObject> queryResult = new ArrayList<>();

        ParseQuery<ParseObject> firstQuery = new ParseQuery<>("Chat"),
                secondQuery = new ParseQuery<>("Chat");
        firstQuery.whereEqualTo("peerA", userID);
        secondQuery.whereEqualTo("peerB", userID);
        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(firstQuery);
        queries.add(secondQuery);
        ParseQuery<ParseObject> finalQuery = ParseQuery.or(queries);
        try {
            queryResult.addAll(finalQuery.find());

            if (queryResult.size() > 0) {
                List<String> channels = new ArrayList<>();

                for (int i = 0; i < queryResult.size(); i++) {
                    channels.add(i, buildChannelString(queryResult.get(i)));
                }

                return channels;
            } else {
                return null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String buildChannelString(ParseObject chat) {
        String peerA = chat.getString("peerA");
        String peerB = chat.getString("peerB");

        if (peerA.compareTo(peerB) > 0) { //peerA is bigger than peerB
            return (peerB + "_" + peerA);
        } else { //peerA is smaller than peerB
            return (peerA + "_" + peerB);
        }
    }

    public void startNewPost(View v) { startActivityForResult(new Intent(this, NewPostActivity.class), 1); }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        refresh(0);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            if (isDrawerOpened) {
                Drawer.closeDrawers();
            } else {
                super.onKeyDown(keyCode, event);
            }
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        // Retrieve the SearchView and plug it into SearchManager
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                isSearching = true;
                searchString = query;
                refresh(mPager.getCurrentItem());
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }


        });
        searchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {

            }

            @Override
            public void onViewDetachedFromWindow(View v) {
                isSearching = false;
                searchString = "";
                refresh(mPager.getCurrentItem());
            }
        });
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mAdView != null) {
            mAdView.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mAdView != null) {
            mAdView.resume();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mAdView != null) {
            mAdView.destroy();
        }
    }
}
