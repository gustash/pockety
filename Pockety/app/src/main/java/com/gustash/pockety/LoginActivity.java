package com.gustash.pockety;

import android.Manifest;
import android.animation.LayoutTransition;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.soundcloud.android.crop.Crop;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import de.hdodenhof.circleimageview.CircleImageView;

public class LoginActivity extends AppCompatActivity {

    private static final int MY_CAMERA_PERMISSIONS_REQUEST = 1;
    private static final int MY_STORAGE_PERMISSIONS_REQUEST = 2;

    LinearLayout registerLayout, loginRegisterLinearLayout;
    ScrollView scrollView;
    EditText etEmail, etPassword, etUsername, etName, etAge, etPhone;
    Button loginButton;
    CircleImageView circleImageView;
    final int REQUEST_CAMERA = 0;
    File f;
    LoadingDialog loadingDialog;
    ImageView logo;
    TextView createAccountTextView;
    FrameLayout logoFrameLayout;
    RelativeLayout inputRelativelayout;
    boolean imageSet = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        registerLayout = (LinearLayout) findViewById(R.id.registerLinearLayout);
        inputRelativelayout = (RelativeLayout) findViewById(R.id.loginRelativeLayout);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        etEmail = (EditText) findViewById(R.id.emailEditText);
        etPassword = (EditText) findViewById(R.id.passwordEditText);
        etUsername = (EditText) findViewById(R.id.usernameEditText);
        etName = (EditText) findViewById(R.id.nameEditText);
        etAge = (EditText) findViewById(R.id.ageEditText);
        etPhone = (EditText) findViewById(R.id.phoneEditText);
        circleImageView = (CircleImageView) findViewById(R.id.avatarCircleImageView);
        loginButton = (Button) findViewById(R.id.loginButton);
        logo = (ImageView) findViewById(R.id.login_logo);
        logoFrameLayout = (FrameLayout) findViewById(R.id.logo_frame_layout);
        loginRegisterLinearLayout = (LinearLayout) findViewById(R.id.login_register_linear_layout);

        createAccountTextView = (TextView) findViewById(R.id.create_account_text_view);
        createAccountTextView.setText(Html.fromHtml(getString(R.string.create_account)));
        createAccountTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showRegister();
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn();
            }
        });

        scrollView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                return false;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_register) {
            showRegister();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showRegister() {
        final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) registerLayout.getLayoutParams();
        final RelativeLayout.LayoutParams logRegParams = (RelativeLayout.LayoutParams)
                loginRegisterLinearLayout.getLayoutParams();
        //ActionMenuItemView registerOption = (ActionMenuItemView) findViewById(R.id.action_register);

        if (registerLayout.getVisibility() != View.VISIBLE) {
            //registerOption.startAnimation(AnimationUtils.loadAnimation(this, R.anim.plus_rotation));
            circleImageView.setVisibility(View.VISIBLE);
            registerLayout.setVisibility(View.VISIBLE);
            createAccountTextView.setText(Html.fromHtml(getString(R.string.already_have_account)));
            loginButton.setText(R.string.register);

            riseLogo();
            params.addRule(RelativeLayout.BELOW, R.id.loginLinearLayout);
            logRegParams.addRule(RelativeLayout.BELOW, R.id.registerLinearLayout);
        } else {
            //registerOption.startAnimation(AnimationUtils.loadAnimation(this, R.anim.remove_rotation));
            circleImageView.setVisibility(View.GONE);
            registerLayout.setVisibility(View.GONE);
            createAccountTextView.setText(Html.fromHtml(getString(R.string.create_account)));
            loginButton.setText(R.string.login);

            lowerLogo();
            params.addRule(RelativeLayout.BELOW, 0);
            logRegParams.addRule(RelativeLayout.BELOW, R.id.loginLinearLayout);
        }
    }

    private void riseLogo() {
        int padding = getResources().getDimensionPixelSize(R.dimen.logo_small_padding);
        int width = getResources().getDimensionPixelSize(R.dimen.logo_small_width);
        logo.setPadding(0, padding, 0, padding);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) logo.getLayoutParams();
        params.gravity = Gravity.LEFT;
        params.width = width;
        logo.setLayoutParams(params);
        inputRelativelayout.setLayoutTransition(null);
    }

    private void lowerLogo() {
        int padding = getResources().getDimensionPixelSize(R.dimen.logo_large_padding);
        int width = getResources().getDimensionPixelSize(R.dimen.logo_large_width);
        logo.setPadding(0, padding, 0, padding);

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) logo.getLayoutParams();
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.width = width;
        logo.setLayoutParams(params);
        inputRelativelayout.setLayoutTransition(new LayoutTransition());
    }

    public void uploadImagePopup(View v) {

        final CharSequence[] items = { "Take Photo", "Choose from Library",
                "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Choose profile picture");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    openCameraIfAllowed();
                } else if (items[item].equals("Choose from Library")) {
                    pickImageIfAllowed();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();

    }

    private void openCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        f = new File(android.os.Environment
                .getExternalStorageDirectory(), "temp.jpg");
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void openCameraIfAllowed() {
        int cameraPermissionCheck = ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.CAMERA);
        int readFilesPermissionCheck = ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (cameraPermissionCheck != PackageManager.PERMISSION_GRANTED
                || readFilesPermissionCheck != PackageManager.PERMISSION_GRANTED) {

            ArrayList<String> permissions = new ArrayList<>();

            if (cameraPermissionCheck != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA);
            }

            if (readFilesPermissionCheck != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }

            ActivityCompat.requestPermissions(LoginActivity.this,
                    permissions.toArray(new String[permissions.size()]),
                    MY_CAMERA_PERMISSIONS_REQUEST);
        } else {
            openCamera();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_CAMERA_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0) {

                    if (grantResults.length == 2
                            && grantResults[0] == PackageManager.PERMISSION_GRANTED
                            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                        openCamera();

                    } else if (grantResults.length == 1
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                        openCamera();

                    } else {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                                .setTitle("Permission denied")
                                .setMessage("You have denied this app access to the camera/storage, please allow it to access it")
                                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });

                        dialog.show();
                    }

                }

                return;
            }

            case MY_STORAGE_PERMISSIONS_REQUEST: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickImage();
                } else {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                            .setTitle("Permission denied")
                            .setMessage("You have denied this app access to the storage, please allow it to access it")
                            .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });

                    dialog.show();
                }
                return;
            }
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void pickImage() {
        Crop.pickImage(this);
    }

    private void pickImageIfAllowed() {
        int readFilesPermissionCheck = ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE);

        if (readFilesPermissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                    MY_STORAGE_PERMISSIONS_REQUEST);
        } else {
            pickImage();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent result) {
        super.onActivityResult(requestCode, resultCode, result);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Uri source = Uri.fromFile(f);
                    beginCrop(source);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == Crop.REQUEST_PICK) {
                beginCrop(result.getData());
            } else if (requestCode == Crop.REQUEST_CROP) {
                try {
                    handleCrop(resultCode, result);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void handleCrop(int resultCode, Intent result) throws IOException {
        if (resultCode == RESULT_OK) {
            if (f != null) {
                f.delete();
                Log.d("File deleted", String.valueOf(f.exists()));
            }
            Uri imageUri = Crop.getOutput(result);
            circleImageView.setImageDrawable(null);
            circleImageView.setImageURI(imageUri);
            imageSet = true;
        } else if (resultCode == Crop.RESULT_ERROR) {
            Toast.makeText(this, Crop.getError(result).getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void beginCrop(Uri source) {
        Uri destination = Uri.fromFile(new File(getCacheDir(), "cropped"));
        Crop.of(source, destination).asSquare().start(this);
    }

    public void logIn() {

        loadingDialog = new LoadingDialog(this);
        loadingDialog.startLoading();

        //Login
        if (registerLayout.getVisibility() != View.VISIBLE) {
            ParseUser.logInInBackground(etUsername.getText().toString(), etPassword.getText().toString(), new LogInCallback() {
                @Override
                public void done(ParseUser parseUser, ParseException e) {
                    loadingDialog.stopLoading();
                    if (e == null) {
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                    } else {
                        Log.d("FAILED LOGIN", e.toString());
                        Toast.makeText(getApplicationContext(), "Login failed. Please check given credentials or try again later", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else { //Register
            register(true);
        }
    }

    private void register(boolean checkForImage) {
        if (!imageSet && checkForImage) {
            loadingDialog.stopLoading();
            showNoImageSetDialog();
            return;
        }

        Bitmap fullBitmap = ((BitmapDrawable)circleImageView.getDrawable()).getBitmap();
        Bitmap bitmap = Bitmap.createScaledBitmap(fullBitmap, 500, 500, true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] image = stream.toByteArray();

        boolean usernameEmpty = etUsername.getText().toString().equals("");
        boolean passwordEmpty = etPassword.getText().toString().equals("");
        boolean emailEmpty = etEmail.getText().toString().equals("");
        boolean nameEmpty = etName.getText().toString().equals("");
        boolean ageEmpty = etAge.getText().toString().equals("");
        boolean phoneEmpty = etPhone.getText().toString().equals("");

        if (!usernameEmpty && !passwordEmpty && !emailEmpty && !nameEmpty && !ageEmpty && !phoneEmpty) {
            String uuid = UUID.randomUUID().toString().replace("-", "");
            final ParseFile file = new ParseFile((uuid + ".jpg"), image);
            file.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        registerUser(file);
                    } else {
                        loadingDialog.stopLoading();
                        Log.d("UPLOAD", e.getCause().toString());
                        Toast.makeText(getApplicationContext(), "Image upload failed. Please try another image", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } else {
            loadingDialog.stopLoading();
            new AlertDialog.Builder(this)
                    .setTitle("Empty field")
                    .setMessage("Please fill in every field and try again.")
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    }

    private void showNoImageSetDialog() {
        new AlertDialog.Builder(this)
                .setTitle("No image set")
                .setMessage(getString(R.string.no_image_selected))
                .setPositiveButton("Go back!", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setNegativeButton("Nah", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        loadingDialog.startLoading();
                        register(false);
                    }
                }).show();
    }

    private void registerUser(ParseFile avatar) {

        ParseUser user = new ParseUser();
        user.setUsername(etUsername.getText().toString());
        user.setPassword(etPassword.getText().toString());
        user.setEmail(etEmail.getText().toString());

        // other fields can be set just like with ParseObject
        user.put("name", etName.getText().toString());
        user.put("age", etAge.getText().toString());
        user.put("phone", etPhone.getText().toString());
        user.put("avatar", avatar);

        user.signUpInBackground(new SignUpCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    ParseUser.logInInBackground(etUsername.getText().toString(), etPassword.getText().toString(), new LogInCallback() {
                        @Override
                        public void done(ParseUser parseUser, ParseException e) {
                            loadingDialog.stopLoading();
                            if (e == null) {
                                finish();
                                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            } else {
                                Toast.makeText(getApplicationContext(), "Login failed. Please login manually", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else if (e.getCode() == ParseException.EMAIL_TAKEN){
                    loadingDialog.stopLoading();
                    Toast.makeText(LoginActivity.this, "E-mail already registered. Please use a different e-mail.", Toast.LENGTH_LONG).show();
                } else if (e.getCode() == ParseException.USERNAME_TAKEN) {
                    loadingDialog.stopLoading();
                    Toast.makeText(LoginActivity.this, "Username already registered. Please use a different username.", Toast.LENGTH_LONG).show();
                } else {
                    loadingDialog.stopLoading();
                    Log.d("REGISTER", e.toString());
                    Toast.makeText(getApplicationContext(), "Can't register at the moment. Try again later.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void showErrorMessage() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(LoginActivity.this);
        dialogBuilder.setMessage("Incorrect user details");
        dialogBuilder.setPositiveButton("Ok", null);
        dialogBuilder.show();
    }
}