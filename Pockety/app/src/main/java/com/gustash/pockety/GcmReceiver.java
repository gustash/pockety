package com.gustash.pockety;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import ibt.ortc.extensibility.GcmOrtcBroadcastReceiver;

public class GcmReceiver extends GcmOrtcBroadcastReceiver {

    private static final String TAG = "GcmReceiver";
    private static final String MESSAGE_PATTERN = "^(.[^_]*)_(.[^-]*)-(.[^_]*)_([\\s\\S]*?)$";
    private static final Pattern messagePattern = Pattern.compile(MESSAGE_PATTERN);

    public GcmReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Received message");
        Bundle extras = intent.getExtras();
        if (extras != null) {
            createNotification(context, extras);
        }
    }

    private String parseOrtcMessage(String ortcMessage) {

        // Automatic ORTC push messages have the following format:
        // <msg_id>_1-1_<message sent by user>

        Matcher parsedMessageMatcher = messagePattern.matcher(ortcMessage);
        String parsedMessage = "";

        try{
            if (parsedMessageMatcher.matches()) {
                parsedMessage = parsedMessageMatcher.group(4);
            }
        } catch (Exception parseException){
            // probably a custom push message, use the received string with no parsing
            parsedMessage = ortcMessage;
        }

        if(parsedMessage == "") {
            // there's something wrong with the message format. Use the unparsed format.
            parsedMessage = ortcMessage;
        }

        return parsedMessage;
    }


    public void createNotification(Context context, Bundle extras)
    {

        MySharedPreferences settings = new MySharedPreferences(context);
        settings.cleanHashIfNeeded();

        String messageString = extras.getString("M");
        String channelId = extras.getString("C");

        if (messageString != "") {

            String objectId = parseOrtcMessage(messageString);
            Log.i(TAG, String.format("Automatic push notification on channel: %s message: %s ", channelId, objectId));

            if (channelId.equals(Config.NOTICE_CHANNEL)) {
                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                Intent notificationIntent = new Intent(context, MainActivity.class);

                String appName = getAppName(context);
                String message = objectId;
                Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.launcher_big_horizontal_shadow);

                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                        Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 9999, notificationIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Notification notification = new NotificationCompat.Builder(context)
                        .setContentTitle("Notice")
                        .setContentText(message)
                        .setSmallIcon(R.mipmap.launcher_big_horizontal_shadow)
                        .setLargeIcon(bitmap)
                        .setContentIntent(pendingIntent)
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setAutoCancel(true).build();
                notificationManager.notify(appName, 9999, notification);

                return;
            }

            if (!settings.wasProcessed(objectId)) {

                settings.processMessage(objectId);

                try {

                    ParseQuery<ParseObject> query = new ParseQuery<>("Message");
                    query.whereEqualTo("objectId", objectId);
                    ParseObject message = query.find().get(0);
                    ParseUser author = message.getParseUser("sender").fetch();
                    if (!author.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) { //If the author is not self then show notification
                        if (!ChatActivity.isInForeground || !ChatActivity.chatId.equals(channelId)) { // Additionally, only show a notification if the chat isn't open in foreground

                            boolean receivesNotifications = PreferenceManager
                                    .getDefaultSharedPreferences(context)
                                    .getBoolean(context.getString
                                            (R.string.pref_notifications_key), true);

                            // parsed message format: <user>:<chat message>
                            if (receivesNotifications) { // Check if the user wants to get notifications
                                String authorName = author.getString("name");
                                String avatarUri = author.getParseFile("avatar").getUrl();
                                Bitmap avatarBmp = new BitmapConverter().execute(avatarUri).get(); //Get the Bitmap from the provided URL

                                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                Intent notificationIntent = new Intent(context, ChatActivity.class);

                                notificationIntent.putExtra("chatId", channelId);
                                Log.d("CHANNEL", channelId);

                                String appName = getAppName(context);

                                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                                        Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                PendingIntent pendingIntent = PendingIntent.getActivity(context, 9999, notificationIntent,
                                        PendingIntent.FLAG_UPDATE_CURRENT);

                                Notification notification = new NotificationCompat.Builder(context)
                                        .setContentTitle(authorName)
                                        .setContentText(message.getString("message"))
                                        .setSmallIcon(R.mipmap.launcher_big_horizontal_shadow)
                                        .setLargeIcon(avatarBmp)
                                        .setContentIntent(pendingIntent)
                                        .setDefaults(Notification.DEFAULT_ALL)
                                        .setPriority(NotificationCompat.PRIORITY_MAX)
                                        .setAutoCancel(true).build();
                                notificationManager.notify(appName, 9999, notification);
                            }
                        } else {
                            ChatActivity.addMessage(message);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Log.d("WAS ALREADY PROCESSED", objectId);
            }

        }
    }

    private String getAppName(Context context)
    {
        CharSequence appName =
                context
                        .getPackageManager()
                        .getApplicationLabel(context.getApplicationInfo());

        return (String)appName;
    }

}