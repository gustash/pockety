package com.gustash.pockety;

import android.content.Context;
import android.util.Log;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import ibt.ortc.api.Ortc;
import ibt.ortc.extensibility.OnConnected;
import ibt.ortc.extensibility.OnDisconnected;
import ibt.ortc.extensibility.OnException;
import ibt.ortc.extensibility.OnMessage;
import ibt.ortc.extensibility.OnReconnected;
import ibt.ortc.extensibility.OnReconnecting;
import ibt.ortc.extensibility.OnSubscribed;
import ibt.ortc.extensibility.OnUnsubscribed;
import ibt.ortc.extensibility.OrtcClient;
import ibt.ortc.extensibility.OrtcFactory;

public class OrtcMethods {

    private static OrtcClient ortcClient;
    //private List<String> subbedChannels;

    private static void prepareSubscriptions(Context context, final List<String> subKeys){
        if (ortcClient == null) {
            Ortc api = new Ortc();
            OrtcFactory factory;
            try {
                factory = api.loadOrtcFactory("IbtRealtimeSJ");
                ortcClient = factory.createClient();
                ortcClient.setApplicationContext(context);
                ortcClient.setGoogleProjectId(Config.PROJECT_ID);

                ortcClient.setClusterUrl(Config.CLUSTER_URL);

                ortcClient.onConnected = new OnConnected() {
                    @Override
                    public void run(OrtcClient sender) {
                        for (int i = 0; i < subKeys.size(); i++) {
                            ortcClient.subscribeWithNotifications(subKeys.get(i), true, new OnMessage() {
                                @Override
                                public void run(OrtcClient ortcClient, String channel, String message) {
                                    Log.d("MESSAGE", "New message recieved: " + message);
                                }
                            });
                        }
                        Log.d("CONNECT", "Connected");
                    }
                };

                ortcClient.onDisconnected = new OnDisconnected() {
                    @Override
                    public void run(OrtcClient sender) {
                        Log.i("CONNECT", "Disconnected");
                    }
                };
                ortcClient.onSubscribed = new OnSubscribed() {
                    @Override
                    public void run(OrtcClient sender, String channel) {
                        Log.i("SUBSCRIBE", "Subscribed to " + channel);
                    }
                };
                ortcClient.onUnsubscribed = new OnUnsubscribed() {
                    @Override
                    public void run(OrtcClient sender, String channel) {
                        Log.i("SUBSCRIBE", "Unsubscribed from " + channel);
                    }
                };
                ortcClient.onException = new OnException() {
                    @Override
                    public void run(OrtcClient sender, Exception exc) {
                        Log.e("ERROR", "Exception " + exc.toString());
                    }
                };
                ortcClient.onReconnected = new OnReconnected() {
                    @Override
                    public void run(OrtcClient sender) {
                        Log.i("CONNECT", "Reconnected");
                    }
                };
                ortcClient.onReconnecting = new OnReconnecting() {
                    @Override
                    public void run(OrtcClient sender) {
                        Log.i("CONNECT", "Reconnecting");
                    }
                };

                ortcClient.connect(Config.APP_KEY, Config.AUTH_TOKEN);

            } catch (Exception e) {
                Log.e("Exception ", e.toString());
            }
        }
    }

    public static void subscribeChannel(String subKey) {
        ortcClient.subscribeWithNotifications(subKey, true, new OnMessage() {
            @Override
            public void run(OrtcClient ortcClient, String channel, String message) {
                Log.d("MESSAGE", "New message recieved: " + message);
            }
        });
    }

    public static void sendMessage(String channel, String messageId) {
        ortcClient.send(channel, messageId);
    }

    public static void alertUserWithoutSubscription(String channel, Context context) {
        OrtcClient tempClient;

        Ortc api = new Ortc();
        OrtcFactory factory;
        try {
            factory = api.loadOrtcFactory("IbtRealtimeSJ");
            tempClient = factory.createClient();
            tempClient.setApplicationContext(context);
            tempClient.setGoogleProjectId(Config.PROJECT_ID);

            tempClient.setClusterUrl(Config.CLUSTER_URL);

            tempClient.onConnected = new OnConnected(){
                @Override
                public void run(OrtcClient sender) {
                    Log.d("CONNECT", "Connected");
                }
            };

            tempClient.onDisconnected = new OnDisconnected() {
                @Override
                public void run(OrtcClient sender) {
                    Log.i("CONNECT", "Disconnected");
                }
            };
            tempClient.onException = new OnException(){
                @Override
                public void run(OrtcClient sender, Exception exc) {
                    Log.e("ERROR", "Exception " + exc.toString());
                }
            };
            tempClient.onReconnected = new OnReconnected(){
                @Override
                public void run(OrtcClient sender) {
                    Log.i("CONNECT", "Reconnected");
                }
            };
            tempClient.onReconnecting = new OnReconnecting(){
                @Override
                public void run(OrtcClient sender) {
                    Log.i("CONNECT", "Reconnecting");
                }
            };

            tempClient.connect(Config.APP_KEY, Config.AUTH_TOKEN);

            tempClient.send(channel, "You have a new message!");

            tempClient.disconnect();

        } catch (Exception e) {
            Log.e("Exception ",e.toString());
        }
    }

    public static void unsubscribeAll(String userID) {
        List<String> subbedChannels = getSubscribedChannels(userID);

        if (subbedChannels != null) {
            for (int i = 0; i < subbedChannels.size(); i++) {
                ortcClient.unsubscribe(subbedChannels.get(i));
                Log.d("UNSUBSCRIBED", subbedChannels.get(i));
            }

            ortcClient.disconnect();
            ortcClient = null;
        } else {
            Log.d("UNSUBSCRIBE", "Error unsubscribing. List is null.");
        }
    }

    public static boolean checkIfSubscribed(String subKey) {
        return ortcClient.isSubscribed(subKey);
    }

    private static List<String> getSubscribedChannels(String userID) {

        final List<ParseObject> queryResult = new ArrayList<>();

        ParseQuery<ParseObject> firstQuery = new ParseQuery<>("Chat"),
                secondQuery = new ParseQuery<>("Chat");
        firstQuery.whereEqualTo("peerA", userID);
        secondQuery.whereEqualTo("peerB", userID);
        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(firstQuery);
        queries.add(secondQuery);
        ParseQuery<ParseObject> finalQuery = ParseQuery.or(queries);
        try {
            queryResult.addAll(finalQuery.find());

            if (queryResult.size() > 0) {
                List<String> channels = new ArrayList<>();
                channels.add(0, Config.NOTICE_CHANNEL);
                channels.add(1, userID);

                for (int i = 0; i < queryResult.size(); i++) {
                    channels.add(buildChannelString(queryResult.get(i)));
                }

                return channels;
            } else {
                return null;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String buildChannelString(ParseObject chat) {
        String peerA = chat.getString("peerA");
        String peerB = chat.getString("peerB");

        if (peerA.compareTo(peerB) > 0) { //peerA is bigger than peerB
            return (peerB + "_" + peerA);
        } else { //peerA is smaller than peerB
            return (peerA + "_" + peerB);
        }
    }

    public static void initializeSubscriptions(Context context, String userID) {
        List<String> subbedChannels = getSubscribedChannels(userID);
        if (subbedChannels != null) {
            prepareSubscriptions(context, subbedChannels);
        } else {
            Log.d("INITIALIZING", "Error initializing subscription. List is null.");
        }
    }
}
