package com.gustash.pockety;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class ChatRoomsActivity extends AppCompatActivity {

    String userID = ParseUser.getCurrentUser().getObjectId();
    LoadingDialog dialog;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    TextView noChatsMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_rooms);

        recyclerView = (RecyclerView) findViewById(R.id.chatRoomsRecyclerView);
        progressBar = (ProgressBar) findViewById(R.id.chat_rooms_progress_bar);
        noChatsMessage = (TextView) findViewById(R.id.no_chats_message);

        dialog = new LoadingDialog(this);

        retrieveChatsList();
    }

    private void retrieveChatsList() {
        ParseQuery<ParseObject> firstQuery = new ParseQuery<>("Chat"),
                                secondQuery = new ParseQuery<>("Chat"),
                                finalQuery;
        firstQuery.whereEqualTo("peerA", userID);
        secondQuery.whereEqualTo("peerB", userID);
        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(firstQuery);
        queries.add(secondQuery);
        finalQuery = ParseQuery.or(queries);
        finalQuery.orderByDescending("updatedAt");
        //dialog.startLoading();
        finalQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (e == null) {
                    if (list.size() > 0) {
                        recyclerView.setVisibility(View.VISIBLE);
                        setRecyclerView(list);
                    } else {
                        noChatsMessage.setVisibility(View.VISIBLE);
                    }
                } else {
                    e.printStackTrace();
                }
                progressBar.setVisibility(View.GONE);
                //dialog.stopLoading();
            }
        });
    }

    private void setRecyclerView(List<ParseObject> chats) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.chatRoomsRecyclerView);
        LinearLayoutManager llm = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(llm);
        ChatRoomAdapter adapter = new ChatRoomAdapter(chats);
        recyclerView.setAdapter(adapter);
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_chat_rooms, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
