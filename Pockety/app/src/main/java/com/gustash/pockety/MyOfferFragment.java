package com.gustash.pockety;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class MyOfferFragment extends Fragment {

    static View rootView;
    static RecyclerView recyclerView;
    static SwipeRefreshLayout swipeRefreshLayout;
    static ParseObject lastEntry;
    static TextView noneNearText;
    static List<ParseObject> loadedPosts;
    LinearLayoutManager llm;
    int fabMargin;
    FloatingActionButton fab;
    public static final java.lang.String ARG_PAGE = "arg_page";
    static LoadingDialog dialog;
    static double latitude, longitude;
    static ProgressBar progressBar;

    public MyOfferFragment() {

    }

    public static MyOfferFragment newInstance(int pageNum) {
        return new MyOfferFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.main_fragment, container, false);
        dialog = new LoadingDialog(rootView.getContext());
        latitude = ParseUser.getCurrentUser().getParseGeoPoint("location").getLatitude();
        longitude = ParseUser.getCurrentUser().getParseGeoPoint("location").getLongitude();
        
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        progressBar = (ProgressBar) rootView.findViewById(R.id.fragment_progress_bar);
        swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeView);
        swipeRefreshLayout.setColorSchemeColors(R.color.primary);
        noneNearText = (TextView) rootView.findViewById(R.id.noItemsInList);
        fab = MainActivity.getFAB();
        fabMargin = getResources().getDimensionPixelSize(R.dimen.fab_margin);
        recyclerView.addOnScrollListener(new MyRecyclerScroll() {
            @Override
            public void show() {
                fab.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2)).start();
            }

            @Override
            public void hide() {
                fab.animate().translationY(fab.getHeight() + fabMargin).setInterpolator(new AccelerateInterpolator(2)).start();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                MainActivity.refresh(1);
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        llm = new LinearLayoutManager(rootView.getContext());

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(llm) {
            @Override
            public void onLoadMore(int current_page) {
                loadMoreData();
            }
        });

        recyclerView.setLayoutManager(llm);

        initializeData();

        return rootView;
    }

    private static void initializeData() {
        swipeRefreshLayout.setVisibility(View.GONE);

        ParseQuery<ParseObject> firstQuery = ParseQuery.getQuery("Post");
        //dialog.startLoading();

        if (MainActivity.isSearching) {
            ParseQuery<ParseObject> secondQuery = ParseQuery.getQuery("Post");
            firstQuery.whereContains("title_lowercase", MainActivity.searchString.toLowerCase());
            secondQuery.whereContains("description_lowercase", MainActivity.searchString.toLowerCase());
            List<ParseQuery<ParseObject>> queries = new ArrayList<>();
            queries.add(firstQuery);
            queries.add(secondQuery);
            ParseQuery<ParseObject> finalQuery = ParseQuery.or(queries);
            finalQuery.whereEqualTo("type", "o");
            finalQuery.whereWithinKilometers("location", new ParseGeoPoint(latitude, longitude), 15);
            finalQuery.orderByDescending("createdAt");
            finalQuery.setLimit(25);
            finalQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if (list.size() > 0) {
                        lastEntry = list.get(list.size() - 1);
                        loadedPosts = list;
                        initializeAdapter(list);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                    } else {
                        swipeRefreshLayout.setVisibility(View.GONE);
                        noneNearText.setVisibility(View.VISIBLE);
                    }
                    //dialog.stopLoading();
                    progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            firstQuery.whereWithinKilometers("location", new ParseGeoPoint(latitude, longitude), 15);
            firstQuery.whereEqualTo("type", "o");
            firstQuery.orderByDescending("createdAt");
            firstQuery.setLimit(25);
            firstQuery.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if (list.size() > 0) {
                        lastEntry = list.get(list.size() - 1);
                        loadedPosts = list;
                        initializeAdapter(list);
                        swipeRefreshLayout.setVisibility(View.VISIBLE);
                    } else {
                        swipeRefreshLayout.setVisibility(View.GONE);
                        noneNearText.setVisibility(View.VISIBLE);
                    }
                    //dialog.stopLoading();
                    progressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    private void loadMoreData() {
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Post");
        query.whereEqualTo("type", "o");
        query.whereWithinKilometers("location", new ParseGeoPoint(latitude, longitude), 15);
        query.whereLessThan("createdAt", lastEntry.getCreatedAt());
        query.setLimit(25);
        query.orderByDescending("createdAt");
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list.size() > 0) {
                    lastEntry = list.get(list.size() - 1);
                    loadedPosts.addAll(list);
                    Log.d("LOADED", "Loaded posts: " + loadedPosts.size());
                    initializeAdapter(loadedPosts);
                    if (loadedPosts.size() >= 25) {
                        llm.scrollToPositionWithOffset(loadedPosts.size() - 25, recyclerView.getHeight());
                    }
                }
            }
        });
    }

    private static void initializeAdapter(List<ParseObject> posts) {
        RVAdapter adapter = new RVAdapter(posts);
        recyclerView.setAdapter(adapter);
    }

    public static View getRootView() {
        return rootView;
    }
}
