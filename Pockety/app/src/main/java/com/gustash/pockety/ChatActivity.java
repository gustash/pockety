package com.gustash.pockety;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChatActivity extends AppCompatActivity {

    static ChatRecyclerViewAdapter adapter;
    ParseUser currentUser = ParseUser.getCurrentUser();
    String firstPeer, secondPeer;
    EditText messageEditText;
    static RecyclerView recyclerView;
    LinearLayoutManager llm;
    boolean isFirstMessage = false;
    int numberOfMsgs = 0;
    LoadingDialog dialog;
    public static boolean isInForeground = false;
    public static String chatId, chatObjectId;
    private static int count;
    private static boolean finishedChatQuery = false,
                           finishedPopulating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        isInForeground = true;
        dialog = new LoadingDialog(this);

        chatId = getIntent().getStringExtra("chatId");
        if (chatId != null) {
            String userID = ParseUser.getCurrentUser().getObjectId();
            OrtcMethods.initializeSubscriptions(this, userID);
            if (!OrtcMethods.checkIfSubscribed(chatId)) {
                OrtcMethods.subscribeChannel(chatId);
            }
            messageEditText = (EditText) findViewById(R.id.etMessage);
            recyclerView = (RecyclerView) findViewById(R.id.rvChat);
            llm = new LinearLayoutManager(this);
            llm.setReverseLayout(true);

            String[] parts = chatId.split("_");
            firstPeer = parts[0]; //first userID
            secondPeer = parts[1]; //second userID

            ParseQuery<ParseObject> query = new ParseQuery<>("_User");
            if (!firstPeer.equals(ParseUser.getCurrentUser().getObjectId())) {
                query.whereEqualTo("objectId", firstPeer);
                Log.d("PEER", firstPeer);
            } else {
                query.whereEqualTo("objectId", secondPeer);
                Log.d("PEER", secondPeer);
            }
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> list, ParseException e) {
                    if (e == null) {
                        if (list.size() > 0) {
                            String name = list.get(0).getString("name");
                            try {
                                getSupportActionBar().setTitle(name);
                            } catch (Exception exc) {
                                exc.printStackTrace();
                            }
                        }
                    }
                }
            });

            populateMessages();
            chatQuery();
        } else {
            new AlertDialog.Builder(this)
                .setTitle("Error")
                .setMessage("Error opening chat. Please try again.")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).create().show();
        }
    }

    private void chatQuery() {
        ParseQuery<ParseObject> firstQuery = new ParseQuery<>("Chat"),
                                secondQuery = new ParseQuery<>("Chat");
        firstQuery.whereEqualTo("peerA", firstPeer);
        firstQuery.whereEqualTo("peerB", secondPeer);
        secondQuery.whereEqualTo("peerA", secondPeer);
        secondQuery.whereEqualTo("peerB", firstPeer);
        List<ParseQuery<ParseObject>> queries = new ArrayList<>();
        queries.add(firstQuery);
        queries.add(secondQuery);
        ParseQuery<ParseObject> finalQuery = ParseQuery.or(queries);

        dialog.startLoading();
        finalQuery.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list.size() != 0) {
                    chatObjectId = list.get(0).getObjectId();
                    setCount();
                }
                dialog.stopLoading();
            }
        });
    }

    private void populateMessages() {
        ParseQuery<ParseObject> query = new ParseQuery<>("Message");
        query.whereEqualTo("chatId", chatId);
        query.include("sender");
        query.setLimit(15);
        query.orderByDescending("createdAt");
        dialog.startLoading();
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                if (list.size() == 0) {
                    isFirstMessage = true;
                }
                setRecyclerAdapter(list);
                finishedPopulating = true;
                dialog.stopLoading();
            }
        });
    }

    private void setRecyclerAdapter(final List<ParseObject> messages) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (finishedPopulating && finishedChatQuery) {
                    recyclerView.setLayoutManager(llm);
                    adapter = new ChatRecyclerViewAdapter(messages);
                    adapter.setHasLoadButton(hasMoreToLoad());
                    recyclerView.setAdapter(adapter);
                } else {
                    setRecyclerAdapter(messages);
                }
            }
        }, 300);
    }

    public void sendMessage(View view) {
        ParseObject chat = new ParseObject("Chat");

        if (isFirstMessage) {
            chat.put("peerA", firstPeer);
            chat.put("peerB", secondPeer);
            chat.put("numberOfMsgs", 0);
            dialog.startLoading();
            chat.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e != null) {
                        toastError(e);
                    }
                    dialog.stopLoading();
                }
            });

            if (!firstPeer.equals(currentUser.getObjectId())) { //firstPeer is the other user, alert him of a new message
                OrtcMethods.alertUserWithoutSubscription(firstPeer, this);
            } else {  //secondPeer is the other user, alert him of a new message
                OrtcMethods.alertUserWithoutSubscription(secondPeer, this);
            }

            chatQuery();

            isFirstMessage = false;
        }

        final String messageText = messageEditText.getText().toString();
        final ParseObject message = new ParseObject("Message");
        message.put("message", messageText);
        message.put("sender", currentUser);
        message.put("chatId", chatId);
        dialog.startLoading();
        message.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    addMessage(message);
                    recyclerView.smoothScrollToPosition(0);
                    messageEditText.setText("");
                    OrtcMethods.sendMessage(chatId, message.getObjectId());
                } else {
                    e.printStackTrace();
                }
                dialog.stopLoading();
            }
        });

        numberOfMsgs++;
        chat = ParseObject.createWithoutData("Chat", chatObjectId);
        chat.increment("numberOfMsgs", 1);

        chat.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e != null) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void loadMoreMessages(Date lastMessageDate) {
        ParseQuery<ParseObject> query = new ParseQuery<>("Message");
        query.whereEqualTo("chatId", chatId);
        query.include("sender");
        query.setLimit(15);
        query.orderByDescending("createdAt");
        query.whereLessThan("createdAt", lastMessageDate);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                adapter.addMessages(list);
                adapter.setHasLoadButton(hasMoreToLoad());
            }
        });
    }

    private static boolean hasMoreToLoad() {
        if (adapter.isHasLoadButton()) {
            return (adapter.getItemCount() - 1) < count;
        } else {
            return (adapter.getItemCount()) < count;
        }
    }

    private static void setCount() {
        ParseQuery<ParseObject> query = new ParseQuery<>("Chat");
        query.whereEqualTo("objectId", chatObjectId);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                count = list.get(0).getNumber("numberOfMsgs").intValue();
                finishedChatQuery = true;
            }
        });
    }

    public static void addMessage(ParseObject message) {
        adapter.addMessage(message);
        recyclerView.smoothScrollToPosition(0);
    }

    private void toastError(ParseException e) {
        Toast.makeText(this, "Error saving chat", Toast.LENGTH_SHORT).show();
        e.printStackTrace();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isInForeground = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isInForeground = true;
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_chat, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
