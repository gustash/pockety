package com.gustash.pockety;

import android.animation.Animator;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.animation.ValueAnimatorCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ShowPostActivity extends AppCompatActivity {

    private String postID, authorID, channel, email, phoneNumber;
    private TextView title, description, date, author, price;
    private ImageView avatar;
    private FloatingActionButton fab, emailFab, SMSFab, callFab;
    private TextView fabLabel, emailFabLabel, SMSFabLabel, callFabLabel;
    private boolean emailAvailable = false, phoneAvailable = false, fabIsOpen = false;
    private Animation openFab, closeFab, dimScreen, undimScreen;
    private RelativeLayout postView;
    private MySharedPreferences mySharedPreferences;
    private Context context;
    //private ImageView filter;
    private AdView mAdView;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_post);

        mAdView = (AdView) findViewById(R.id.showPostActivityAdView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mAdView.loadAd(adRequest);

        context = this;
        mySharedPreferences = new MySharedPreferences(this);

        title = (TextView) findViewById(R.id.titleTextView);
        description = (TextView) findViewById(R.id.descriptionTextView);
        date = (TextView) findViewById(R.id.dateTextView);
        author = (TextView) findViewById(R.id.authorTextView);
        price = (TextView) findViewById(R.id.priceTextView);
        avatar = (ImageView) findViewById(R.id.avatarShowPost);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fabLabel = (TextView) findViewById(R.id.big_fab_label);
        emailFab = (FloatingActionButton) findViewById(R.id.fabEmail);
        emailFabLabel = (TextView) findViewById(R.id.fab_email_label);
        SMSFab = (FloatingActionButton) findViewById(R.id.fabSMS);
        SMSFabLabel = (TextView) findViewById(R.id.fab_sms_label);
        callFab = (FloatingActionButton) findViewById(R.id.fabCall);
        callFabLabel = (TextView) findViewById(R.id.fab_call_label);
        postView = (RelativeLayout) findViewById(R.id.post_view);
        progressBar = (ProgressBar) findViewById(R.id.show_post_progress_bar);
        //scrollView = (ScrollView) findViewById(R.id.show_post_scroll_view);
        //filter = (ImageView) findViewById(R.id.dimmingFilter);

        openFab = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_open);
        closeFab = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fab_close);
        dimScreen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.dim_fade_in);
        undimScreen = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.dim_fade_out);

        postID = getIntent().getExtras().getString("postID");
        getInterfaceData();

        /*filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fabIsOpen) {
                    closeFab();
                }
            }
        });*/
    }

    private void getInterfaceData() {
        ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("Post");
        query.getInBackground(postID, new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    populateInterface(parseObject);
                }
            }
        });
    }

    private void populateInterface(final ParseObject returnedPost) {
        title.setText(returnedPost.getString("title"));
        description.setText(returnedPost.getString("description"));
        date.setText(convertDateToString(returnedPost.getCreatedAt()));

        if (returnedPost.getBoolean("showEmail")) {
            emailAvailable = true;
        }

        if (returnedPost.getBoolean("showPhone")) {
            phoneAvailable = true;
        }

        returnedPost.getParseObject("parent").fetchIfNeededInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    authorID = parseObject.getObjectId();
                    if (!authorID.equals(ParseUser.getCurrentUser().getObjectId())) {
                        fab.setVisibility(View.VISIBLE);
                    }
                    author.setText(parseObject.getString("name"));
                    ParseFile parseFile = parseObject.getParseFile("avatar");
                    if (parseFile != null) {
                        Picasso.with(getApplicationContext()).load(parseFile.getUrl()).into(avatar);
                    }
                    try {
                        email = parseObject.getString("email");
                        phoneNumber = parseObject.getString("phone");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                progressBar.setVisibility(View.GONE);
                postView.setVisibility(View.VISIBLE);
            }
        });
        price.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format
                        (returnedPost.getDouble("price")));

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean fabWillExpand = false;

                if (emailAvailable) {
                    emailFab.setVisibility(View.VISIBLE);
                    emailFabLabel.setVisibility(View.VISIBLE);
                    fabWillExpand = true;

                    emailFab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("message/rfc822");
                            i.putExtra(Intent.EXTRA_EMAIL,
                                    new String[]{email});
                            //i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                            //i.putExtra(Intent.EXTRA_TEXT   , "body of email");
                            try {
                                startActivity(Intent.createChooser(i, "Send e-mail..."));
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(ShowPostActivity.this,
                                        "There are no email clients installed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    pullFabsDown();
                }

                if (phoneAvailable) {
                    callFab.setVisibility(View.VISIBLE);
                    callFabLabel.setVisibility(View.VISIBLE);
                    SMSFab.setVisibility(View.VISIBLE);
                    SMSFabLabel.setVisibility(View.VISIBLE);
                    fabWillExpand = true;

                    SMSFab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                startActivity(new Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.fromParts("sms", phoneNumber, null)));
                            } catch (Exception e) {
                                Toast.makeText(ShowPostActivity.this,
                                        "Can't send SMS",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    callFab.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                startActivity(new Intent(
                                        Intent.ACTION_DIAL,
                                        Uri.fromParts("tel", phoneNumber, null)));
                            } catch (Exception e) {
                                Toast.makeText(ShowPostActivity.this,
                                        "Can't open dialer",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                if (!mySharedPreferences.clickedMessageOnce() && fabWillExpand) {
                    Toast.makeText(ShowPostActivity.this,
                            "You can click this button again to send an in-app message",
                            Toast.LENGTH_SHORT).show();
                    mySharedPreferences.setClickedMessageOnce();
                }

                if (!fabWillExpand) {
                    message();
                } else {
                    if (!fabIsOpen) {

                        if (emailAvailable) {
                            openFab(emailFab);
                            openFab(emailFabLabel);
                        }
                        if (phoneAvailable) {
                            openFab(callFab);
                            openFab(callFabLabel);
                            openFab(SMSFab);
                            openFab(SMSFabLabel);
                        }
                    } else {
                        closeFab();
                        message();
                    }
                }
            }
        });

        postView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fabIsOpen) {
                    closeFab();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (!fabIsOpen) {
            super.onBackPressed();
        } else {
            closeFab();
        }
    }

    /* private void dimScreen() {
        filter.setVisibility(View.VISIBLE);
        filter.startAnimation(dimScreen);
    } */

    /* private void undimScreen() {
        filter.setVisibility(View.GONE);
        filter.startAnimation(undimScreen);
    } */

    private void openFab(View button) {
        if (!fabIsOpen) {
            fab.setBackgroundTintList(ContextCompat.getColorStateList(ShowPostActivity.this,
                    R.color.group_fab_color_list_open));
            fabLabel.startAnimation(openFab);
            fabLabel.setVisibility(View.VISIBLE);
        }

        button.startAnimation(openFab);
        //dimScreen();
        fabIsOpen = true;
    }

    private void closeFab() {
        fab.setBackgroundTintList(ContextCompat.getColorStateList(ShowPostActivity.this,
                R.color.group_fab_color_list_collapsed));
        fabLabel.startAnimation(closeFab);
        fabLabel.setVisibility(View.GONE);

        //undimScreen();
        if (emailFab.getVisibility() == View.VISIBLE) {
            emailFab.startAnimation(closeFab);
            emailFab.setVisibility(View.GONE);
            emailFabLabel.startAnimation(closeFab);
            emailFabLabel.setVisibility(View.GONE);
        }

        if (callFab.getVisibility() == View.VISIBLE) {
            callFab.startAnimation(closeFab);
            callFab.setVisibility(View.GONE);
            callFabLabel.startAnimation(closeFab);
            callFabLabel.setVisibility(View.GONE);
        }

        if (SMSFab.getVisibility() == View.VISIBLE) {
            SMSFab.startAnimation(closeFab);
            SMSFab.setVisibility(View.GONE);
            SMSFabLabel.startAnimation(closeFab);
            SMSFabLabel.setVisibility(View.GONE);
        }

        fabIsOpen = false;
    }

    private void pullFabsDown() {
        int margin = (int) (52 * Resources.getSystem().getDisplayMetrics().density);

        ViewGroup.MarginLayoutParams callFabParams = (ViewGroup.MarginLayoutParams)
                callFab.getLayoutParams();
        callFabParams.setMargins(callFabParams.leftMargin,
                callFabParams.topMargin, callFabParams.rightMargin,
                callFabParams.bottomMargin - margin);

        ViewGroup.MarginLayoutParams SMSFabParams = (ViewGroup.MarginLayoutParams)
                SMSFab.getLayoutParams();
        SMSFabParams.setMargins(SMSFabParams.leftMargin,
                SMSFabParams.topMargin, SMSFabParams.rightMargin,
                SMSFabParams.bottomMargin - margin);
    }

    private String convertDateToString(Date date) {
        DateFormat df = DateFormat.getDateInstance();
        return df.format(date);
    }


    public void message() {
        String userID = ParseUser.getCurrentUser().getObjectId();

        if (userID.compareTo(authorID) > 0) { //userID is bigger than authorID
            channel = (authorID + "_" + userID);
        } else { //userID is smaller than authorID
            channel = (userID + "_" + authorID);
        }

        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra("chatId", channel);
        startActivity(intent);
    }
}
