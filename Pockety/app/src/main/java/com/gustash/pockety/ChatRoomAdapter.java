package com.gustash.pockety;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ChatRoomAdapter extends RecyclerView.Adapter<ChatRoomAdapter.JobViewHolder>{

    public static class JobViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView message;
        ImageView author;

        JobViewHolder(final View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.chatRoomsCardView);
            author = (ImageView)itemView.findViewById(R.id.chatUserRoomsAvatar);
            message = (TextView)itemView.findViewById(R.id.message);
        }
    }

    List<ParseObject> chats;
    String[] chatIds;
    View v;
    ParseFile parseFile;

    ChatRoomAdapter(List<ParseObject> chats){
        this.chats = chats;
        chatIds = new String[chats.size()];
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public JobViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_rooms_item, viewGroup, false);
        JobViewHolder jvh = new JobViewHolder(v);
        return jvh;
    }

    @Override
    public void onBindViewHolder(final JobViewHolder jobViewHolder, final int i) {
        final String peer, chatId;
        String userID = ParseUser.getCurrentUser().getObjectId();
        String peerA = chats.get(i).getString("peerA");
        String peerB = chats.get(i).getString("peerB");

        if (peerA.compareTo(peerB) > 0) { //peerA is bigger than peerB
            chatIds[i] = (peerB + "_" + peerA);
        } else { //peerA is smaller than peerB
            chatIds[i] = (peerA + "_" + peerB);
        }

        ParseQuery<ParseObject> query = new ParseQuery<>("Message");
        query.whereEqualTo("chatId", chatIds[i]);
        query.orderByDescending("createdAt");
        query.setLimit(1);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                jobViewHolder.message.setText(list.get(0).getString("message"));
            }
        });

        if (peerA.equals(userID)) {
            peer = peerB;
        } else {
            peer = peerA;
        }

        ParseQuery<ParseUser> userQuery = ParseUser.getQuery();
        userQuery.getInBackground(peer, new GetCallback<ParseUser>() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (e == null) {
                    parseFile = parseUser.getParseFile("avatar");
                    if (parseFile != null) {
                        Picasso.with(jobViewHolder.itemView.getContext())
                                .load(parseFile.getUrl())
                                .into(jobViewHolder.author);
                    }
                } else {
                    e.printStackTrace();
                }
            }
    });
        jobViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context currentContext = v.getContext();
                Intent intent = new Intent(currentContext, ChatActivity.class);
                intent.putExtra("chatId", chatIds[i]);
                currentContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (chats != null) {
            return chats.size();
        } else {
            return 0;
        }
    }
}
