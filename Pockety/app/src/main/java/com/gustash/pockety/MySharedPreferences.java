package com.gustash.pockety;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.ArraySet;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class MySharedPreferences {
    static final long ONE_MINUTE_IN_MILLIS = 60000; //millisecs

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public MySharedPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void saveLocation(LatLng location) {
        float lat = (float) location.latitude;
        float lon = (float) location.longitude;

        editor.putFloat("latitude", lat);
        editor.putFloat("longitude", lon);
        editor.apply();
    }

    public LatLng getLocation() {
        float lat = sharedPreferences.getFloat("latitude", 0);
        float lon = sharedPreferences.getFloat("longitude", 0);

        return new LatLng(lat, lon);
    }

    public void saveNotifications(boolean doesRecieve) {
        editor.putBoolean("recievesNotifications", doesRecieve);
        editor.apply();
    }

    public boolean recievesNotifications() {
        return sharedPreferences.getBoolean("recievesNotifications", true);
    }

    public void processMessage(String objectId) {
        Set<String> processedMessages = sharedPreferences.getStringSet("processedMessages", new HashSet<String>());
        processedMessages.add(objectId);

        Date currentDate = new Date(System.currentTimeMillis());
        long expireDate = currentDate.getTime() + (5 * ONE_MINUTE_IN_MILLIS); // Add 5 minutes to current date

        editor.putStringSet("processedMessages", processedMessages);
        editor.putLong("expireDate", expireDate);
        editor.apply();
    }

    public boolean wasProcessed(String objectId) {
        Set<String> processedMessages = sharedPreferences.getStringSet("processedMessages", new HashSet<String>());

        return processedMessages.contains(objectId); // If the message was processed returns true, otherwise returns false
    }

    public void cleanHashIfNeeded() {
        long currentDate = new Date(System.currentTimeMillis()).getTime();
        long expireDate = sharedPreferences.getLong("expireDate", 0);

        if (expireDate != 0 && currentDate >= expireDate) {
            editor.remove("processedMessages");
            editor.apply();
            Log.d("CLEANED HASH", "TRUE");
        } else {
            Log.d("CLEANED HASH", "FALSE");
        }
    }

    public boolean clickedMessageOnce() {
        return sharedPreferences.getBoolean("clickedMessageOnce", false);
    }

    public void setClickedMessageOnce() {
        editor.putBoolean("clickedMessageOnce", true);
        editor.apply();
    }

    public boolean isFirstTimeRunning() {
        return sharedPreferences.getBoolean("firstTimeRunning", true);
    }

    public void setRanForFirstTime() {
        editor.putBoolean("firstTimeRunning", false);
        editor.apply();
    }
}
