package com.gustash.pockety;

import android.Manifest;
import android.annotation.TargetApi;
import android.view.View.OnClickListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.internal.PlaceImpl;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final int PERMISSION_REQUEST_CODE = 1;
    private static final int LOCATION_REQUEST_CODE = 2;

    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private MarkerOptions markerOptions;
    private Toolbar toolbar;
    private MySharedPreferences mySharedPreferences;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private SupportPlaceAutocompleteFragment autocompleteFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        toolbar = (Toolbar) findViewById(R.id.maps_tool_bar);
        setSupportActionBar(toolbar);

        markerOptions = new MarkerOptions();
        mySharedPreferences = new MySharedPreferences(this);

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        getMap();
    }

    private void prepareAutocomplete() {
        autocompleteFragment = (SupportPlaceAutocompleteFragment)
                getSupportFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("PLACE", place.getName().toString());
                setSupportActionBar(toolbar);

                mMap.clear();

                markerOptions.position(place.getLatLng());
                mMap.addMarker(markerOptions);

                CameraPosition.Builder cameraPosition = new CameraPosition.Builder
                        (mMap.getCameraPosition());

                cameraPosition.target(place.getLatLng());
                cameraPosition.zoom(15.0f);

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition.build()));
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("PLACE", "An error occurred: " + status);
            }
        });

    }

    private boolean hasLocationPermission() {
        return checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void getMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        if (mapFragment == null) {
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        prepareAutocomplete();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            askLocationPermission();
        }

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();

                markerOptions.position(latLng);
                mMap.addMarker(markerOptions);

                CameraPosition.Builder cameraPosition = new CameraPosition.Builder
                        (mMap.getCameraPosition());

                cameraPosition.target(latLng);
                cameraPosition.zoom(15.0f);

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition.build()));
                autocompleteFragment.setText(getLocationString(latLng));
            }
        });

        if (mMap.isMyLocationEnabled()) {
            mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                @Override
                public boolean onMyLocationButtonClick() {
                    if (mLastLocation != null) {
                        double lat = mLastLocation.getLatitude();
                        double lon = mLastLocation.getLongitude();

                        LatLng latLng = new LatLng(lat, lon);

                        mMap.clear();
                        markerOptions.position(latLng);
                        mMap.addMarker(markerOptions);

                        CameraPosition.Builder cameraPosition = new CameraPosition.Builder
                                (mMap.getCameraPosition());

                        cameraPosition.target(latLng);
                        cameraPosition.zoom(15.0f);

                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition.build()));
                        autocompleteFragment.setText(getLocationString(latLng));
                    }

                    return true;
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_maps, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_accept: {
                showAcceptDialog();
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void showAcceptDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Set location");
        dialog.setMessage("Do you want to set this as your location? \n(You can change this later in Settings)");
        dialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                double lat = markerOptions.getPosition().latitude;
                double lon = markerOptions.getPosition().longitude;

                final LoadingDialog loadingDialog = new LoadingDialog(MapsActivity.this);
                loadingDialog.startLoading();

                ParseUser user = ParseUser.getCurrentUser();
                user.put("location", new ParseGeoPoint(lat, lon));
                user.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        loadingDialog.stopLoading();
                        if (e == null) {
                            Intent intent = new Intent(MapsActivity.this, MainActivity.class)
                                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                                            Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                        } else {
                            e.printStackTrace();
                            Toast.makeText(MapsActivity.this, "Can't save location right now. Try again later",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults[requestCode] == PackageManager.PERMISSION_GRANTED) {
                if (!isProviderEnabled()) {
                    showEnableLocationDialog();
                }
                mMap.setMyLocationEnabled(true);
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void askLocationPermission() {
        if (!hasLocationPermission()) {

            String[] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, permissions, PERMISSION_REQUEST_CODE);
        } else {
            if (isProviderEnabled()) {
                mMap.setMyLocationEnabled(true);
            } else {
                showEnableLocationDialog();
            }
        }
    }

    private void setLocation(Location location) {
        mLastLocation = location;

        if (location != null) {
            mLastLocation = location;
        } else {

            //TODO: check if location is enabled on device
            if (mMap.isMyLocationEnabled()) {
                mMap.setMyLocationEnabled(false);
            }

            Toast.makeText(this, "Not able to get your location automatically", Toast.LENGTH_SHORT).show();
        }
    }

    private void showEnableLocationDialog() {
        AlertDialog.Builder locationDialogBuilder = new AlertDialog.Builder(this);
        locationDialogBuilder.setTitle("GPS disaled");
        locationDialogBuilder.setMessage("Your GPS seems to be disabled. Activate it to enable automatic location?");
        locationDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (hasLocationPermission()) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_REQUEST_CODE);
                    }
                } else {
                    startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_REQUEST_CODE);
                }
            }
        });
        locationDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mMap.setMyLocationEnabled(false);
            }
        });
        locationDialogBuilder.show();
    }

    private boolean isProviderEnabled() {
        LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
                locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void getLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isProviderEnabled()) {
                if (hasLocationPermission()) {
                    setLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
                }
            } else {
                mMap.setMyLocationEnabled(false);
            }
        } else {
            if (isProviderEnabled()) {
                setLocation(LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient));
            } else {
                mMap.setMyLocationEnabled(false);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        autocompleteFragment.onActivityResult(requestCode, resultCode, data);


        if (requestCode == LOCATION_REQUEST_CODE) {
            getMap();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    @Override
    public void onLocationChanged(Location location) {
        setLocation(location);
    }

    private String getLocationString(LatLng latLng) {
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = gcd.getFromLocation(latLng.latitude, latLng.longitude, 1);

            if (addresses.size() > 0) {
                return addresses.get(0).getLocality();
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }
}
