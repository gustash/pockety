package com.gustash.pockety;

public class Config {

    public static final String APP_KEY = "ug7j4y";
    public static final String AUTH_TOKEN = "authToken";
    public static final String CLUSTER_URL = "http://ortc-developers.realtime.co/server/2.1/";
    public static final String PROJECT_ID = "585094966593";
    public static final String NOTICE_CHANNEL = "notices";

}
