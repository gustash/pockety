package com.gustash.pockety;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;

public class PreferencesActivity extends AppCompatPreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyPreferenceFragment mPreferenceFragment = new MyPreferenceFragment();
        getFragmentManager().beginTransaction().replace(android.R.id.content, mPreferenceFragment).commit();
    }

    public static class MyPreferenceFragment extends PreferenceFragment {

        public Preference changePasswordButton, changeLocationButton, receiveNotificationsSwitch;
        private LoadingDialog loadingDialog;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            loadingDialog = new LoadingDialog(getActivity());

            changePasswordButton = findPreference(getString(R.string.pref_password_key));
            changeLocationButton = findPreference(getString(R.string.pref_current_location_key));
            receiveNotificationsSwitch = findPreference(getString(R.string.pref_notifications_key));

            changePasswordButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    requestPasswordReset();

                    return false;
                }
            });
            changeLocationButton.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    startActivity(new Intent(getActivity(), MapsActivity.class));

                    return false;
                }
            });
            receiveNotificationsSwitch.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object notifEnabled) {
                    if ((boolean) notifEnabled) {
                        preference.setSummary(getString(R.string.pref_notifications_summary_disable));
                    } else {
                        preference.setSummary(getString(R.string.pref_notifications_summary_enable));
                    }

                    return true;
                }
            });
        }

        private void requestPasswordReset() {
            new AlertDialog.Builder(getActivity())
                    .setTitle("Are you sure?")
                    .setMessage("You'll be sent an e-mail to set a new password, but you'll also be logged out.")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            proceedWithPasswordReset();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }

        private void proceedWithPasswordReset() {
            ParseUser.requestPasswordResetInBackground(ParseUser.getCurrentUser().getEmail(),
                    new RequestPasswordResetCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                                new AlertDialog.Builder(getActivity())
                                        .setTitle("E-mail sent")
                                        .setMessage("You've been sent an e-mail. You will now be logged out.")
                                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                final String userID = ParseUser.getCurrentUser().getObjectId();

                                                loadingDialog.startLoading();
                                                ParseUser.logOutInBackground(new LogOutCallback() {
                                                    @Override
                                                    public void done(ParseException e) {
                                                        if (e == null) {
                                                            redirectToLogin(userID);
                                                        } else {
                                                            Log.d("LOGOUT", e.getMessage());
                                                            Toast.makeText(getActivity(),
                                                                    "Error logging out.",
                                                                    Toast.LENGTH_SHORT).show();
                                                        }
                                                    }
                                                });
                                            }
                                        }).show();
                            } else {
                                Log.d("PASSWORD", e.getMessage());
                                Toast.makeText(getActivity(),
                                        "Error requesting password reset. Try again later.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }

        private void redirectToLogin(String userID) {
            loadingDialog.stopLoading();

            OrtcMethods.initializeSubscriptions(getActivity(), userID);
            OrtcMethods.unsubscribeAll(userID);

            Intent intent = new Intent(getActivity(), LoginActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|
                            Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }
}
