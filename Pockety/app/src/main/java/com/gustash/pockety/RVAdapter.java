package com.gustash.pockety;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.GetCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.JobViewHolder>{

    public static class JobViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        TextView locality, title, price;
        ImageView author;

        JobViewHolder(final View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cardView);
            title = (TextView)itemView.findViewById(R.id.title);
            price = (TextView)itemView.findViewById(R.id.price);
            author = (ImageView)itemView.findViewById(R.id.cardAvatar);
            locality = (TextView)itemView.findViewById(R.id.locality);
        }
    }

    List<ParseObject> posts;
    ParseGeoPoint location;
    String localityString;
    Geocoder gcd;
    View v;
    List<Address> addresses;
    ParseFile parseFile;

    RVAdapter(List<ParseObject> posts){
        this.posts = posts;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public JobViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        JobViewHolder jvh = new JobViewHolder(v);
        return jvh;
    }

    @Override
    public void onBindViewHolder(final JobViewHolder jobViewHolder, final int i) {
        jobViewHolder.title.setText(posts.get(i).getString("title"));
        jobViewHolder.price.setText(String.valueOf(posts.get(i).getInt("price")));
        jobViewHolder.price.setText(NumberFormat.getCurrencyInstance(Locale.ITALY).format
                (posts.get(i).getDouble("price")));
        jobViewHolder.locality.setText(posts.get(i).getString("locality"));
        posts.get(i).getParseObject("parent").fetchIfNeededInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject parseObject, ParseException e) {
                if (e == null) {
                    parseFile = parseObject.getParseFile("avatar");
                    if (parseFile != null) {
                        Picasso.with(jobViewHolder.itemView.getContext())
                                .load(parseFile.getUrl())
                                .into(jobViewHolder.author);
                    }
                }
            }
        });
        final int position = i;
        jobViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context currentContext = v.getContext();
                Intent intent = new Intent(currentContext, ShowPostActivity.class);
                intent.putExtra("postID", posts.get(position).getObjectId());
                currentContext.startActivity(intent);
            }
        });
    }

    private String convertDateToString(Date date) {
        DateFormat df = DateFormat.getDateInstance();
        return df.format(date);
    }

    @Override
    public int getItemCount() {
        if (posts != null) {
            return posts.size();
        } else {
            return 0;
        }
    }
}
